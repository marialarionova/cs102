def encrypt_caesar(plaintext):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("")
    ''
    """
    # PUT YOUR CODE HERE
    ciphertext = ""
    for i in range (0, len(plaintext)):
        if ((ord(plaintext[i]) >= 88 and ord(plaintext[i]) <=90) or
           (ord(plaintext[i]) >= 120 and ord(plaintext[i]) <=122)) :
            ciphertext += chr(ord(plaintext[i]) - 23)
        else:
            ciphertext += chr(ord(plaintext[i]) + 3)
    return ciphertext


def decrypt_caesar(ciphertext):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("")
    ''
    """
    # PUT YOUR CODE HERE
    return plaintext

print(encrypt_caesar("PYTHON"))
print(encrypt_caesar("python"))
print(ord("X"))
print(ord("x"))
print(ord("A"))
print(ord("a"))