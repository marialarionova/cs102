def encrypt_caesar(plaintext):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ""
    for i in range(len(plaintext)):
        if ((ord(plaintext[i]) >= ord("X") and ord(plaintext[i]) <= ord("Z")) or
           (ord(plaintext[i]) >= ord("x") and ord(plaintext[i]) <= ord("z"))) :
            ciphertext += chr(ord(plaintext[i]) - 23)
        else:
            ciphertext += chr(ord(plaintext[i]) + 3)
    return ciphertext    


def decrypt_caesar(ciphertext):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ""
    for i in range (len(ciphertext)):
        if ((ord(ciphertext[i]) >= ord("A") and ord(ciphertext[i]) <= ord("C")) or
           (ord(ciphertext[i]) >= ord("a") and ord(ciphertext[i]) <= ord("c"))) :
            plaintext += chr(ord(ciphertext[i]) + 23)
        else:
            plaintext += chr(ord(ciphertext[i]) - 3)
    return plaintext     


def encrypt_caesar_shift(plaintext, shift):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar_shift("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar_shift("python")
    'sbwkrq'
    >>> encrypt_caesar_shift("")
    ''
    """
    ciphertext = ""
    for i in range (len(plaintext)):
        if ((ord(plaintext[i]) >= ord("Z") - shift and ord(plaintext[i]) < ord("Z")) or
           (ord(plaintext[i]) >= ord("z") - shift and ord(plaintext[i]) < ord("z"))) :
            ciphertext += chr(ord(plaintext[i]) - (26 - shift))
        else:
            ciphertext += chr(ord(plaintext[i]) + shift)
    return ciphertext


def decrypt_caesar_shift(ciphertext, shift):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar_shift("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar_shift("sbwkrq")
    'python'
    >>> decrypt_caesar_shift("")
    ''
    """    
    plaintext = ""
    for i in range (len(ciphertext)):
        if ((ord(ciphertext[i]) >= ord("A") and ord(ciphertext[i]) < (ord("A") + shift)) or
           (ord(ciphertext[i]) >= ord("a") and ord(ciphertext[i]) < (ord("a") + shift))) :
            plaintext += chr(ord(ciphertext[i]) + (26 - shift))
        else:
            plaintext += chr(ord(ciphertext[i]) - shift)
    return plaintext