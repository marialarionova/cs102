def encrypt_caesar(plaintext):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("")
    ''
    """
    ciphertext = ""
    for i in range(len(plaintext)):
        if ((ord(plaintext[i]) >= ord("A") and ord(plaintext[i]) <= ord("W")) or
           (ord(plaintext[i]) >= ord("a") and ord(plaintext[i]) <= ord("w"))) :
            ciphertext += chr(ord(plaintext[i]) + 3) 
        elif ((ord(plaintext[i]) >= ord("X") and ord(plaintext[i]) <= ord("Z")) or
           (ord(plaintext[i]) >= ord("x") and ord(plaintext[i]) <= ord("z"))) :
            ciphertext += chr(ord(plaintext[i]) - 23)
        else:
            ciphertext += plaintext[i]
    return ciphertext    


def decrypt_caesar(ciphertext):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("")
    ''
    """
    plaintext = ""
    for i in range (len(ciphertext)):
        if ((ord(ciphertext[i]) >= ord("D") and ord(ciphertext[i]) <= ord("Z")) or
           (ord(ciphertext[i]) >= ord("d") and ord(ciphertext[i]) <= ord("z"))) :
            plaintext += chr(ord(ciphertext[i]) - 3)
        elif ((ord(ciphertext[i]) >= ord("A") and ord(ciphertext[i]) <= ord("C")) or
           (ord(ciphertext[i]) >= ord("a") and ord(ciphertext[i]) <= ord("c"))) :
            plaintext += chr(ord(ciphertext[i]) + 23)
        else:
            plaintext += ciphertext[i]
    return plaintext     


def encrypt_caesar_shift(plaintext, shift):
    """
    Encrypts plaintext using a Caesar cipher.

    >>> encrypt_caesar_shift("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar_shift("python")
    'sbwkrq'
    >>> encrypt_caesar_shift("")
    ''
    """
    ciphertext = ""
    for i in range (len(plaintext)):
        if ((ord(plaintext[i]) >= ord("A") and ord(plaintext[i]) <= ord("Z") - shift) or
           (ord(plaintext[i]) >= ord("a") and ord(plaintext[i]) <= ord("z") - shift)) :
            ciphertext += chr(ord(plaintext[i]) + shift)
        elif ((ord(plaintext[i]) > ord("Z") - shift and ord(plaintext[i]) <= ord("Z")) or
           (ord(plaintext[i]) > ord("z") - shift and ord(plaintext[i]) <= ord("z"))) :
            ciphertext += chr(ord(plaintext[i]) - (26 - shift))
        else:
            ciphertext += plaintext[i]
    return ciphertext


def decrypt_caesar_shift(ciphertext, shift):
    """
    Decrypts a ciphertext using a Caesar cipher.

    >>> decrypt_caesar_shift("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar_shift("sbwkrq")
    'python'
    >>> decrypt_caesar_shift("")
    ''
    """    
    plaintext = ""
    for i in range (len(ciphertext)):
        if ((ord(ciphertext[i]) >= ord("A") + shift and ord(ciphertext[i]) <= ord("Z")) or
           (ord(ciphertext[i]) >= ord("a") + shift and ord(ciphertext[i]) <= ord("z"))) :
            plaintext += chr(ord(ciphertext[i]) - shift)
        elif ((ord(ciphertext[i]) >= ord("A") and ord(ciphertext[i]) < (ord("A") + shift)) or
           (ord(ciphertext[i]) >= ord("a") and ord(ciphertext[i]) < (ord("a") + shift))) :
            plaintext += chr(ord(ciphertext[i]) + (26 - shift))
        else:
            plaintext += ciphertext[i]
    return plaintext
