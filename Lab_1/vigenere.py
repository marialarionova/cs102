def encrypt_vigenere(plaintext, keyword):
    """
    Encrypts plaintext using a Vigenere cipher.

    >>> encrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> encrypt_vigenere("python", "a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    """
    ciphertext = ""
    while len(plaintext) > len(keyword) :
        keyword += keyword
    if len(plaintext) < len(keyword) :
        keyword = keyword[:len(plaintext)]
    for i in range (len(plaintext)):
        if ord("A") <= ord(plaintext[i]) <= ord("Z") :
            shift = ord(keyword[i]) - ord("A")
        else: 
            shift = ord(keyword[i]) - ord("a")        
        if (ord(plaintext[i]) > (ord("Z") - shift) and (ord(plaintext[i]) <= ord("Z")) or
           (ord(plaintext[i]) > (ord("z") - shift) and (ord(plaintext[i]) <= ord("z")))) :
            ciphertext += chr(ord(plaintext[i]) - (26 - shift))
        else:
            ciphertext += chr(ord(plaintext[i]) + shift)
    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    """
    Decrypts a ciphertext using a Vigenere cipher.

    >>> decrypt_vigenere("PYTHON", "A")
    'PYTHON'
    >>> decrypt_vigenere("python", "a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    """
    plaintext = ""
    while len(ciphertext) > len(keyword) :
        keyword += keyword
    if len(ciphertext) < len(keyword) :
        keyword = keyword[:len(ciphertext)]
    for i in range (len(ciphertext)):
        if ord("A") <= ord(ciphertext[i]) <= ord("Z") :
            shift = ord(keyword[i]) - ord("A")
        else: 
            shift = ord(keyword[i]) - ord("a")         
        if ((ord(ciphertext[i]) >= ord("A") and ord(ciphertext[i]) < (ord("A") + shift)) or
           (ord(ciphertext[i]) >= ord("a") and ord(ciphertext[i]) < (ord("a") + shift))) :
            plaintext += chr(ord(ciphertext[i]) + (26 - shift))
        else:
            plaintext += chr(ord(ciphertext[i]) - shift)
    return plaintext